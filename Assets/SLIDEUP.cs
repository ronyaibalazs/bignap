﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SLIDEUP : MonoBehaviour {

	private Vector3 screenPoint;
	private Vector3 offset;
	public Vector3 Maxposition;
	private AudioSource Napaudio;
	public float Volume;

	void Start () {
		Maxposition = transform.position;
		Napaudio = gameObject.GetComponent<AudioSource> ();
	}


	void Update () {
		Volume = transform.position.y - 4f;

		Napaudio.volume = Volume;


	}



	void OnMouseDown()
	{
		screenPoint = Camera.main.WorldToScreenPoint(transform.position);
		offset =  transform.position - Camera.main.ScreenToWorldPoint(new Vector3(transform.position.x, Input.mousePosition.y,screenPoint.z));
	}

	void OnMouseDrag()
	{
		Vector3 curScreenPoint = new Vector3(transform.position.x, Input.mousePosition.y, screenPoint.z);
		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		if ((curPosition.y < (Maxposition.y + 1)) && (curPosition.y > (Maxposition.y))) {
			transform.position = curPosition;
		}
	}
}
