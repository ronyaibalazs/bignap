﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beerclick : MonoBehaviour {

	public bool IsBeerClicked
	;
	public Animator anim;



	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();

	}

	// Update is called once per frame
	void Update () {
		anim.SetBool ("IsBeerClicked",IsBeerClicked);
	}
	void OnMouseDown ()
	{
		if (!IsBeerClicked) {
			IsBeerClicked = true;
		
	}
	}
	void AnimationEnded()
	{
		if (IsBeerClicked) {
			IsBeerClicked = false;
		}
	}

				}