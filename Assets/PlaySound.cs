﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

    public AudioClip SoundToPlay;
	public AudioClip SoundToPlay2;
	public float StartingVolume;
    public float Volume;
	public float Volume2;
    private AudioSource PlayAudio;
    
    void Start()
    {
		PlayAudio = GetComponent<AudioSource>();
    }

    void PlaySoundd()
    {
        
        {
            PlayAudio.PlayOneShot(SoundToPlay, Volume);
           
        }
    }
	void PlaySoundd2()
	{

		{
			PlayAudio.PlayOneShot(SoundToPlay2, Volume2);

		}
	}
	void StopSound()
	{

		{
			PlayAudio.Stop ();

		}
	}
	//void IncreaseVolume()
	//{
	//	Volume += 0.2f;
	//}

	//void ResetVolume()
	//{
	//	Volume = StartingVolume;
	//}

}
