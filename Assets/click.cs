﻿using UnityEngine;
using System.Collections;

public class click : MonoBehaviour {

	private Animator anim;
	public bool awake;
	public float SpiderSpeed;
	public float SpiderMaxSpeed = 30f ;
	public bool goingLeft;
	public float moveHorizontal;
	public bool Full;



	private Rigidbody2D rb2d;

	void OnMouseDown ()
	{
		if (!awake) {
			awake = true;
		} else {
			awake = false;
		}
	}
	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		GameObject[] remaining = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject clone in remaining) {
			if(clone.name == "Spider(Clone)"){
				GameObject.Destroy(clone);
			}
		}

	}
	
	// Update is called once per frame
	void Update () {
		anim.SetBool ("Awake",awake);
		anim.SetBool ("Left",goingLeft);
		anim.SetFloat ("Speed", Mathf.Abs(rb2d.velocity.x));
		anim.SetBool ("Full", Full);

		//if (Input.GetAxis ("Horizontal") < -0.1f) {
		//	goingLeft = true;
		//}

		//	if (Input.GetAxis ("Horizontal") > 0.1f) {
		//	goingLeft = false;
		//	}

	
	}
	void FixedUpdate()
	{
		if (awake) {
			if (Input.GetMouseButton (0)) {

				Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));
				if (mouseWorldPosition.x < transform.position.x) {
					goingLeft = true;
					moveHorizontal = -1f;
				} else {
					goingLeft = false;
					moveHorizontal = 1f;
				}

				Vector2 movement = new Vector2 (moveHorizontal, 0);
				rb2d.AddForce (movement * SpiderSpeed);

				if (rb2d.velocity.x > SpiderMaxSpeed) {
					rb2d.velocity = new Vector2 (SpiderMaxSpeed, rb2d.velocity.y);
				}
				if (rb2d.velocity.x < -SpiderMaxSpeed) {
					rb2d.velocity = new Vector2 (-SpiderMaxSpeed, rb2d.velocity.y);
				}
			}
		}
	}
	void OnTriggerStay2D(Collider2D target) {

		if (target.tag == "Energy" && !awake) {
			Full = true;

		}
		if (target.tag == "Energy2" && !awake && Full) {
			

		}
	}
}
