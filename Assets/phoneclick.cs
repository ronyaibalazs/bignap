﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class phoneclick : MonoBehaviour {


	public bool IsPhoneClicked;
	public int PhoneState = 0;
	public Animator anim;



	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();

	}

	// Update is called once per frame
	void Update () {
		anim.SetInteger ("PhoneState",PhoneState);
	}
	void OnMouseDown ()
	{
		if (!IsPhoneClicked) {
			IsPhoneClicked = true;
			PhoneState ++;

		
	}
}

	void AnimationEnded()
	{
		if (IsPhoneClicked) {
			IsPhoneClicked = false;
	}
			}
}