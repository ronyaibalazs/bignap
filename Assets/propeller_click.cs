﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class propeller_click : MonoBehaviour {

	public bool IsButtonPushed;
	public int propellerState;
	public Animator anim;



	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();

	}

	// Update is called once per frame
	void Update () {
		anim.SetBool ("ButtonPushed",IsButtonPushed);
		anim.SetInteger ("PropellerState", propellerState);

		if (propellerState > 3)
		{
			propellerState = 0;
		}
			
	}
	void OnMouseDown ()
	{
		if (!IsButtonPushed) {
			IsButtonPushed = true;
			propellerState++;
	}
}


	void AnimationEnded()
	{
		if (IsButtonPushed) {
			IsButtonPushed = false;
		}
	}
}