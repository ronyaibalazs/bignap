﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clickable : MonoBehaviour {

	public bool IsTvOn;
	public Animator anim;



	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		anim.SetBool ("IsTelevisionOn",IsTvOn);
	}
	void OnMouseDown ()
	{
		if (!IsTvOn) {
			IsTvOn = true;
		} else {
			IsTvOn = false;
		}
	}

}
